<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/test.css') }}" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<img src="BLOW2.png" width="100%" height="100%">
    <div class="row wrapper">
        <nav class="col-sm-9 page-navigation">
            <a href="#" class="col-sm-1 page-navigation__blow">blow</a>
            <button class="button-home">home</button>
            <a href="#" class="col-sm-1 page-navigation__about">about</a>
            <a href="#" class="col-sm-1 page-navigation__work">work</a>
            <a href="#" class="col-sm-3 page-navigation__contact">contact</a>
        </nav>
        <button class="explore-work">EXPLORE WORK</button>
        <div class="information-text">
            <span class="information-text__left">
                get in touch
            </span>
            <span class="information-text__have">
                have project in mind
            </span>
        </div>
        <div class="col-sm-12 small-logo">
            <a href="#"><img src="small-logo.png"></a>
        </div>
    </div>
</body>
</html>